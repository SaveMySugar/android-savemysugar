/*
 * SaveMySugar - Exchange messages using the distance between phone calls
 *
 * Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * adouble with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package it.ao2.savemysugar.util;

import android.util.Log;
import android.widget.TextView;

import java.util.Locale;

public final class TimeLog {
    private TimeLog() {
    }

    public static void log(String tag, TextView textView, String message) {
        double now = System.currentTimeMillis() / 1000.;
        String nowString =  String.format(Locale.US, "%.6f", now);
        textView.append(nowString + " " + message);
        Log.d(tag, nowString + " " + message);
    }
}
