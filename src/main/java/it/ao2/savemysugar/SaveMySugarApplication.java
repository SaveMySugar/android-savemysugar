/*
 * SaveMySugar - Exchange messages using the distance between phone calls
 *
 * Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * adouble with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package it.ao2.savemysugar;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import it.ao2.util.morse.MorseDistanceModulator;
import it.ao2.util.morse.MorseTranslator;

public class SaveMySugarApplication extends Application {

    private static final String TAG = "SaveMySugar";
    private MorseDistanceModulator mModulator;
    private MorseTranslator mTranslator;

    @Override
    public void onCreate() {
        super.onCreate();

        mModulator = new MorseDistanceModulator();
        updateModulatorParams();

        mTranslator = new MorseTranslator();
    }

    public void updateModulatorParams() {
        String defaultCallSetupTimeMin = Double.toString(mModulator.DEFAULT_PERIOD_MIN);
        String defaultCallSetupTimeMax = Double.toString(mModulator.DEFAULT_PERIOD_MAX);
        String defaultRingTimeMin = Double.toString(mModulator.DEFAULT_PULSE_MIN);
        String defaultRingTimeMax = Double.toString(mModulator.DEFAULT_PULSE_MAX);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        double newCallSetupTimeMin = Double.parseDouble(sharedPrefs.getString("call_setup_time_min", defaultCallSetupTimeMin));
        double newCallSetupTimeMax = Double.parseDouble(sharedPrefs.getString("call_setup_time_max", defaultCallSetupTimeMax));
        double newRingTimeMin = Double.parseDouble(sharedPrefs.getString("ring_time_min", defaultRingTimeMin));
        double newRingTimeMax = Double.parseDouble(sharedPrefs.getString("ring_time_max", defaultRingTimeMax));

        Log.d(TAG, "Call setup time min: " + newCallSetupTimeMin + "\n");
        Log.d(TAG, "Call setup time max: " + newCallSetupTimeMax + "\n");
        Log.d(TAG, "Ring time min: " + newRingTimeMin + "\n");
        Log.d(TAG, "Ring time max: " + newRingTimeMax + "\n");

        // Analog modems don't like to dial a new call immediately after they
        // hung up the previous one; an extra delay of about one ring time
        // makes them happy: use newRingTimeMax as the interSymbolDistance.
        double interSymbolDistance = newRingTimeMax;

        mModulator.setParameters(newCallSetupTimeMin, newCallSetupTimeMax,
                                 newRingTimeMin, newRingTimeMax,
                                 interSymbolDistance);
    }

    public MorseDistanceModulator getModulator() {
        return mModulator;
    }

    public MorseTranslator getTranslator() {
        return mTranslator;
    }
}
